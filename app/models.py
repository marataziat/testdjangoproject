from django.db import models

class Test(models.Model):
    name = models.CharField(max_length=255)

class Testrelated(models.Model):
    name = models.CharField(max_length=255)
    tests = models.ManyToManyField(Test, related_name='tests')
