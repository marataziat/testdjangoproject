from django.contrib import admin
from . import models

class TestrelatedInline(admin.TabularInline):
    extra = 0
    model = models.Test.tests.through
    filter_horizontal = ('name',)

class TestAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    search_fields = ('name',)

    inlines = [
        TestrelatedInline,
    ]

# Register your models here.
admin.site.register(models.Test, TestAdmin)
